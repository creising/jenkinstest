# # use a node base image
# FROM node:7-onbuild

# # set maintainer
# LABEL maintainer "creising@gmail.com"

# # set a health check
# HEALTHCHECK --interval=5s \
#             --timeout=5s \
#             CMD curl -f http://127.0.0.1:8000 || exit 1

# # tell docker what port to expose
# EXPOSE 8000

FROM node:8.9-alpine as base
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install
COPY . .
RUN npm run test

FROM base
ENV NODE_ENV production
WORKDIR /usr/src/app
EXPOSE 8000
CMD npm start