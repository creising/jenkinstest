// load the http module
var http = require('http');
const MyTest = require('./somefunction');

// configure our HTTP server
var server = http.createServer(function (request, response) {
  const mytest = new MyTest();
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("My text is: " + mytest.sayHello() + " one more");
  //response.end(mytest.sayHello());
});

// listen on localhost:8000
server.listen(8000);
console.log("Server listening at http://127.0.0.1:8000/");