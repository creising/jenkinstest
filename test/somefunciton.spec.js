const assert = require('assert');
const MyClass = require('../somefunction');

describe("MyClass", () => {
  describe("Say Hello", () => {
    it("Should start with hello", () => {
      const my = new MyClass();
      const s = my.sayHello();
      const startWithHello = s.startsWith("Hello");
      assert.equal(startWithHello, true);
    });
  });
});